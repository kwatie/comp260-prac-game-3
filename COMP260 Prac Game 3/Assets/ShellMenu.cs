﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using System.Collections.Generic;

public class ShellMenu : MonoBehaviour {

	public GameObject shellPanel;
	public GameObject optionsPanel;
	public Dropdown qualityDropdown;
	public Dropdown resolutionDropdown;
	public Toggle fullscreenToggle;
	public Slider volumeSlider;
	private bool paused = true;

	// Use this for initialization
	void Start () {
		//options panel is initially hidden
		optionsPanel.SetActive(false);
		SetPaused (paused);

		//populate the list of video quality levels
		qualityDropdown.ClearOptions();
		List<string> names = new List<string> ();
		for (int i = 0; i < QualitySettings.names.Length; i++) {
			names.Add (QualitySettings.names [i]);
		}
		qualityDropdown.AddOptions (names);
	}

	public void OnPressedOptions() {
		//show the options pane & hide the shell panel
		shellPanel.SetActive (false);
		optionsPanel.SetActive (true);

		// select the current quality value
		qualityDropdown.value = QualitySettings.GetQualityLevel();
	}

	public void OnPressedCancel() {
		//return to the shell menu
		shellPanel.SetActive (true);
		optionsPanel.SetActive (false);
	}

	public void OnPressedApply() {
		//return to the shell menu
		shellPanel.SetActive (true);
		optionsPanel.SetActive (false);

		// apply the changes
		QualitySettings.SetQualityLevel(qualityDropdown.value);
	}
	
	// Update is called once per frame
	void Update () {
		if (!paused && Input.GetKeyDown (KeyCode.Escape)) {
			SetPaused(true);
		}
	}

	private void SetPaused(bool p) {
		paused = p;
		shellPanel.SetActive (paused);
		Time.timeScale = paused ? 0 : 1;
	}

	public void OnPressedPlay() {
		//resumes the game
		SetPaused (false);
	}

	public void OnPressedQuit() {
		//quit the game
		Application.Quit ();
	}
}
